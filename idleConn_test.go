package rpcLimitPool

import (
	"testing"
	"time"
)

func Test_conn1(t *testing.T) {
	newIdleConn(nil, 5*time.Second)
	time.Sleep(10 * time.Second)
}

func Test_conn2(t *testing.T) {
	c := newIdleConn(nil, 5*time.Second)
	time.Sleep(2 * time.Second)
	c.get()
	time.Sleep(5 * time.Second)
}
