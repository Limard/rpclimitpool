package rpcLimitPool

import (
	"fmt"
	"net/http"
	"net/rpc"
	"testing"
	"time"
)

func TestTimeout(t *testing.T) {
	now := time.Now()
	timer := time.NewTimer(5 * time.Second)

	go func() {
		for {
			select {
			case <-timer.C:
				fmt.Println(time.Now().Sub(now))
			}
		}
	}()

	time.Sleep(2 * time.Second)
	timer.Reset(5 * time.Second)
	//timer.Stop()  // 不会触发 <- timer.C

	time.Sleep(20 * time.Second)
}

func TestRpcLimitPool(t *testing.T) {
	go rpcServer()
	time.Sleep(1 * time.Second)

	pool := NewRpcLimitPool(&RpcLimitPool{
		NewConn: func() (client *rpc.Client, err error) {
			return rpc.Dial("tcp", "127.0.0.1:3211")
		},
		LimitCap:    2,
		IdleTimeout: 10 * time.Second,
	})

	for i := 0; i < 10; i++ {
		fmt.Println(i, "get wait:", time.Now().String())
		c, _ := pool.Get()
		fmt.Println(i, "get:", time.Now())
		go func(c *rpc.Client, i int) {
			time.Sleep(1 * time.Second)
			if i == 4 {
				pool.Put(c, fmt.Errorf(""))
			} else {
				pool.Put(c, nil)
			}
			fmt.Println(i, "put:", time.Now())
		}(c, i)
	}

	time.Sleep(30 * time.Second)
}

func rpcServer() {
	rpc.HandleHTTP()
	http.ListenAndServe(`127.0.0.1:3211`, nil)
}
