package rpcLimitPool

import (
	"net/rpc"
	"sync"
	"time"
)

type idleConn struct {
	c  *rpc.Client
	t  *time.Timer
	f  chan int // finish timer
	mu sync.Mutex
}

func newIdleConn(client *rpc.Client, timeout time.Duration) *idleConn {
	if client == nil {
		return nil
	}

	c := &idleConn{
		c: client,
		t: time.NewTimer(timeout),
		f: make(chan int, 1),
	}
	go c.timeout()
	return c
}

func (t *idleConn) timeout() {
	select {
	case <-t.t.C:
		//fmt.Println("<-t.t.C: ", time.Now())
		t.mu.Lock()
		if t.c != nil {
			t.c.Close()
			t.c = nil
		}
		t.mu.Unlock()
	case <-t.f:
		//fmt.Println("<-t.f: ", time.Now())
	}
}

func (t *idleConn) get() *rpc.Client {
	if t == nil || t.c == nil {
		return nil
	}

	t.mu.Lock()
	t.t.Stop()
	t.f <- 0
	client := t.c
	t.c = nil
	t.mu.Unlock()

	return client
}
