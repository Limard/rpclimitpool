package rpcLimitPool

import (
	"errors"
	"net/rpc"
	"time"
)

type RpcLimitPool struct {
	LimitCap    int           //限制的最大连接数
	IdleTimeout time.Duration //连接最大空闲时间，超过该事件则将失效
	NewConn     func() (*rpc.Client, error)

	idleConns chan *idleConn
}

// NewChannelPool 初始化连接
func NewRpcLimitPool(p *RpcLimitPool) *RpcLimitPool {
	p.idleConns = make(chan *idleConn, p.LimitCap)
	for i := 0; i < p.LimitCap; i++ {
		p.idleConns <- &idleConn{c: nil}
	}
	return p
}

// Get 从pool中取一个连接
func (t *RpcLimitPool) Get() (*rpc.Client, error) {
	if t.idleConns == nil {
		return nil, errors.New("pool is closed")
	}

	select {
	case wrapConn := <-t.idleConns:
		if c := wrapConn.get(); c != nil {
			return c, nil
		}
		return t.NewConn()
	}
}

// Put 将连接放回pool中
func (t *RpcLimitPool) Put(conn *rpc.Client) error {
	if t.idleConns == nil {
		if conn != nil {
			conn.Close()
			return nil
		}
	}

	select {
	case t.idleConns <- newIdleConn(conn, t.IdleTimeout):
		return nil
	default: //连接池已满，直接关闭该连接（异常）
		if conn != nil {
			return conn.Close()
		} else {
			return nil
		}
	}
}

// Release 释放连接池中所有连接
func (t *RpcLimitPool) Release() {
	conns := t.idleConns
	t.idleConns = nil

	if conns == nil {
		return
	}

	close(conns)
	for wrapConn := range conns {
		if c := wrapConn.get(); c != nil {
			c.Close()
		}
	}
}

// Len 未被分配的链接数
func (t *RpcLimitPool) Len() int {
	return len(t.idleConns)
}
